"""Shows PEER_INDEX_TABLE of remote RIB snapshot.

Example usage:
  $ python3 show_remote_peer_index_table.py ris rrc24 2022-07-01
  61573 138.122.60.1
  852 154.11.15.28
  263702 168.195.130.2
  263702 168.195.131.1
  ...
"""
import argparse
import bz2
import datetime
import errno
import urllib3.util.retry
import requests
import socket
import sys
import zlib

from typing import Dict, Optional, Iterator, Tuple

def get_retry_session() -> requests.Session:
  """Generates a Session object with useful retry behavior.

  Returns:
    The configures session.
  """
  s = requests.Session()
  retries = urllib3.util.retry.Retry(
      total=5,
      backoff_factor=0.1,
      status_forcelist=[500, 502, 503, 504],
      raise_on_status=True,
  )
  s.mount("http://", requests.adapters.HTTPAdapter(max_retries=retries))
  s.mount("https://", requests.adapters.HTTPAdapter(max_retries=retries))
  return s


def valid_date(s: str) -> datetime.datetime:
  """Validates and parses the date string.

  Args:
    s: the string to validate and parse.

  Returns:
    datetime.datetime object of te date.

  Raises:
    ValueError if not a yyyy-mm-dd formatted date.
  """
  try:
    return datetime.datetime.strptime(s, "%Y-%m-%d")
  except ValueError:
    msg = f"not a valid date: {s}"
    raise argparse.ArgumentTypeError(msg)


def valid_project(s: str) -> str:
  """Validates and parses the provided project.

  Args:
    s: the string to validate and parse.

  Returns:
    the project string.

  Raises:
    ValueError if not any of the possible project strings.
  """

  if s in ["ris", "routeviews"]:
    return s
  else:
    msg = f"{s} is neither 'ris' nor 'routeviews'."
    raise argparse.ArgumentTypeError(msg)


def _transform_coll(s: str) -> str:
  """fixes issues with the rv2 collector repo name."""
  if s.strip() == "routeviews2":
    return ""
  return s.strip()


def find_file(kwargs: Dict[str, str]) -> Optional[str]:
  """Uses kwargs to determien and find the requestd remote file.

  Args:
    kwargs: dict of arguments containing DATE, PROJ, and COLL strings.

  Returns:
    The url of the file, if found, else None.
  """
  s = get_retry_session()
  ts = kwargs["DATE"]
  if kwargs["PROJ"] == "ris":
    url = f"https://data.ris.ripe.net/{kwargs['COLL']}/{ts.strftime('%Y')}.{ts.strftime('%m')}/bview.{ts.strftime('%Y%m%d')}.0000.gz"  # pylint: disable=line-too-long
    resp = s.head(url)
    if resp.status_code == requests.codes.ok:
      return url

  else:  # for routevies some files are off by a minute or two ...
    for i in range(6):
      url = f"http://archive.routeviews.org/{kwargs['COLL']}/bgpdata/{ts.strftime('%Y')}.{ts.strftime('%m')}/RIBS/rib.{ts.strftime('%Y%m%d')}.000{i}.bz2"  # pylint: disable=line-too-long
      resp = s.head(url)
      if resp.status_code == requests.codes.ok:
        return url
  return None


class MinimalStreamDecompressor:
  """Very scrappy and minimal decompressor class."""

  def __init__(self, url):
    if url.endswith(".gz"):
      self.dec = zlib.decompressobj(32 + zlib.MAX_WBITS)
    else:
      self.dec = bz2.BZ2Decompressor()
    self.chunks = requests.get(url, stream=True).raw
    self.leftovers = bytearray()

  def read(self, n: int) -> bytes:
    """Reads n decoded bytes from the input stream.

    In theory, this code is broken beyond measure as it omits a gazillion
    edge cases; however, we oly read a few bytes from any file anyways so we
    never handle any kind of EOF :P

    Args:
      n: Number of dwcoded Bytes to read.

    Returns:
      An array of n decoded Bytes.
    """
    while len(self.leftovers) < n:
      self.leftovers.extend(self.dec.decompress(next(self.chunks)))
    wb, self.leftovers = self.leftovers[:n], self.leftovers[n:]
    return wb


def yield_peers(url: str) -> Iterator[Optional[Tuple[str, int, str]]]:
  """Parses the PEER_INDEX_TABLE and yields peers.

  Args:
    url: The url to read the RIB snapshot from.

  Yields:
    three tuples containing the vname, asn, and peer_addr of the peer.

  Returns:
    None

  Raises:
    EOFError: If the file does not contain a PEER_INDEX_TABLE in the
      first 30 records.
  """
  ifile = MinimalStreamDecompressor(url)
  for _ in range(30):  # artificial boundary ...
    # parse common header information
    mrt_ts = int.from_bytes(ifile.read(4), "big", signed=False)
    mrt_type = int.from_bytes(ifile.read(2), "big", signed=False)
    mrt_subtype = int.from_bytes(ifile.read(2), "big", signed=False)
    mrt_len = int.from_bytes(ifile.read(4), "big", signed=False)
    # print(mrt_ts, mrt_type, mrt_subtype, mrt_len)
    # ignore everything that's not the PEER_INDEX_TABLE
    if mrt_type != 13 or mrt_subtype != 1:
      _ = ifile.read(mrt_len)
      continue

    # this is the PEER_INDEX_TABLE
    mrt_coll_id = int.from_bytes(ifile.read(4), "big", signed=False)
    mrt_vname_len = int.from_bytes(ifile.read(2), "big", signed=False)

    # set the vname if available
    mrt_vname = ""
    if mrt_vname_len != 0:
      mrt_vname = ifile.read(mrt_vname_len).decode("utf-8")

    # read the peer list
    mrt_peer_cnt = int.from_bytes(ifile.read(2), "big", signed=False)
    for _ in range(mrt_peer_cnt):
      mrt_peer_type = int.from_bytes(ifile.read(1), "big", signed=False)
      mrt_long_asn = mrt_peer_type & (1 << 1) != 0
      mrt_proto_6 = mrt_peer_type & (1 << 0) != 0
      mrt_peer_bgp_id = int.from_bytes(ifile.read(4), "big", signed=False)
      mrt_peer_addr = socket.inet_ntop(
          socket.AF_INET6 if mrt_proto_6 else socket.AF_INET,
          ifile.read((4, 16)[mrt_proto_6])
      )
      mrt_peer_asn = int.from_bytes(
          ifile.read((2, 4)[mrt_long_asn]), "big", signed=False
      )
      yield mrt_vname, mrt_peer_asn, mrt_peer_addr
    return
  raise EOFError("Couldn't find any PEER_INDEX_TABLE in the first 30 records.")


if __name__ == "__main__":
  ap = argparse.ArgumentParser()
  ap.add_argument(
      "PROJ",
      help="the project that hosts the collector",
      type=valid_project,
  )
  ap.add_argument(
      "COLL",
      help="the collector for which you want to see the peers",
      type=_transform_coll,
  )
  ap.add_argument("DATE", help="UTC+0 Date in format yyyy-mm-dd",
                  type=valid_date)
  kwargs = vars(ap.parse_args())

  url = find_file(kwargs)
  if url:
    for (_, peer_asn, peer_addr) in yield_peers(url):
      try:
        print(peer_asn, peer_addr, sep=" ")
      except IOError as e:
        if e.errno == errno.EPIPE:  # e.g., when piping to head
            sys.exit(0)
  else:
    print("Did not find a RIB snapshot at any of the potential urls.")

