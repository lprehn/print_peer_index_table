"""Shows all PEER_INDEX_TABLEs for all active collectors.

Example usage:
  $ python3 show_all_peers_remote.py 2022-08-01
  route-views2 0 0.0.0.0
  route-views2 3356 4.68.4.46
  route-views2 14061 5.101.110.2
  route-views2 7018 12.0.1.63
  route-views2 57866 37.139.139.17
  route-views2 63927 43.226.4.1
  ...
"""
import datetime
import errno
import argparse
import show_remote_peer_index_table as rptable
import sys

from typing import Dict

BGP_STREAM_META_API = "https://broker.bgpstream.caida.org/v2/data?"


def lookup_active_collectors(
    start: int) -> Dict[str, Dict[str, Dict[str, str]]]:
  """Looks up active collector via BGPStream Broker API.

  Args:
    start: the start utc+0 timestamp.

  Returns:
    A dictionary that encodes all collectors.
  """
  # setting up the request
  s = rptable.get_retry_session()
  params = {"type": "ribs", "intervals": str(start) + "," + str(start)}
  resp = s.get(BGP_STREAM_META_API, params=params, timeout=0.5)

  # checking if the call succeeded
  return resp.json()


def show_peer_list(colls: Dict[str, Dict[str, Dict[str, str]]]) -> None:
  """Prints the PEER_INDEX_TABLE for all collectors.

  Args:
    colls: A dict that encodes all collectors, see
      lookup_active_collectors(...).
  """
  for coll in colls["data"]["resources"]:
    for _, asn, addr in rptable.yield_peers(coll["url"]):
      try:
        print(coll["collector"], asn, addr)
      except IOError as e:
        if e.errno == errno.EPIPE:  # e.g., when piping to head
            sys.exit(0)

parser = argparse.ArgumentParser()
parser.add_argument(
    "day",
    help="the date for which peer lookup will be run",
    type=rptable.valid_date
)
args = vars(parser.parse_args())

colls = lookup_active_collectors(
    int(args["day"].replace(tzinfo=datetime.timezone.utc).timestamp()))
show_peer_list(colls)
