This is a scrappy implementation of a remote peer table lookup for Routeviews and RIPE RIS data.
As the code only downloads a few bytes (however much is needed to read the entire peer table 
record) instead of the whole RIB snapshot, it provides a snappy response. 

in total, there are two scripts:

1) show_remote_peer_index_table.py \<PROJ\> \<COLL\> \<DATE\>
2) show_all_peers_remote.py \<DATE\>

where \<PROJ\> is either "ris" or "routeviews", the \<COLL\> is the the name of a collector of
that project (e.g., rrc02 or routeviews2), and \<DATE\> is the utc date from which the midnight RIB
snapshot is used for the lookup in yyyy-mm-dd format. 

To run them, please first install the requirements via:

~~~~
pip3 install -r requirements.txt
~~~~

The first script shows all peers of the defined rib file, e.g.:

~~~
$ python3 show_remote_peer_index_table.py ris rrc24 2022-07-01
61573 138.122.60.1
852 154.11.15.28
263702 168.195.130.2
263702 168.195.131.1
266309 170.79.176.100
270014 177.221.140.2
269956 189.126.8.1
6057 200.40.162.202
28000 200.7.84.35
28000 2001:13c7:7001:4032::35
852 2001:56b:2:21::3
64271 2401:d9c0::1
6057 2800:a0:401:c828:a2ca:2000:0:ca
263702 2803:3b80:1ee3:1000::1
263702 2803:3b80:6000::3
147028 2803:3b80:d:1020::2
265721 2803:4dc0:50::1
270014 2803:a9e0::1
64116 2803:cd60:1:1::1
269956 2803:fae0::bd7e:801
266309 2804:35d4:b0da::f0c0
46997 2a03:90c0:186::11a
147028 45.228.210.221
265721 45.65.244.1
46997 5.188.4.211
~~~

The second script iterates over all ris and routeviews collectors and shows
all their peer table entires, e.g.:

~~~
$ python3 show_all_peers_remote.py 2022-08-01 | head -n 10
route-views2 0 0.0.0.0
route-views2 3356 4.68.4.46
route-views2 14061 5.101.110.2
route-views2 7018 12.0.1.63
route-views2 57866 37.139.139.17
route-views2 63927 43.226.4.1
route-views2 22652 45.61.0.85
route-views2 1299 62.115.128.137
route-views2 6939 64.71.137.241
route-views2 812 64.71.255.61
~~~

Please note that the head -n 10 limited the output to 10 out of 3116 lines. 


### Caveats & Gotchas

- The scripts show the entire peer index table, i.e., they include entries like
"0 0.0.0.0".
- The code always uses the yyyy-mm-dd 00:00 UTC+0 RIB; however, it is aware of
some repo issues, e.g., (1) if the 00:00 file is not there it also checks for
filenames up to 00:06 and (2) it correctly sets the repo name for the routeviews2
collector.
- The code is nowhere near production quality, it worked well for my day-to-day 
research, yet YMMV. 
